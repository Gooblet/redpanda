'''
RedPandas is an enhanced concept of the python pandas module. 
The goal is to streamline the data analysis process and include some reporting capabilities
'''

from pandas import DataFrame, read_csv
import pandas as pd
import matplotlib.pyplot as plt
#TODO - find out how to use arguments in python. . . this should be basic but I can't do it....
#TODO - add additional functions as callable objects and different imports
#TODO - list of arguments I want: head(), foot(), sorting, numpy functions [visuals and such]
#TODO - goal of this is to be able to streamline a number of functions of pandas and make it easier to understand the data and visualize it
#TODO - export results to csv/xlsx to email to boss

# def start():
#     global file
#     file = input("filename")
#     importFile()


def importFile():
    '''
    requires user to input filename. prompted. then prints the file
    '''
    file = input("Enter Filename: ")
    df = pd.read_csv(file)
    print(df)


importFile()